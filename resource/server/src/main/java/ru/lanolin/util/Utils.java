package ru.lanolin.util;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.Map;

public class Utils {
    public static final int MAXSIZE = 256;
    private static final ScriptEngine engine = new ScriptEngineManager().getEngineByName("javascript");

    /**
     * Метод, который преобразует {@code JSON} строку в {@link Map}
     */
    public static Map<String, Object> parseJSON(String element) {
        Map<String, Object> result = null;
        try { result = (Map<String, Object>) engine.eval("Java.asJSONCompatible(" + element + ")"); } 
        catch (Exception e) { System.err.println("Внимание!!! Введен неверный формат JSON строки. Исправьте или не продолжайте работать."); }
        return result;
    }

    public static ByteBuffer convertObject2Buffer(Object obj) throws IOException {
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream outputStreamWriter = new ObjectOutputStream(arrayOutputStream);
        outputStreamWriter.writeObject(obj);
        outputStreamWriter.flush();
        ByteBuffer b = ByteBuffer.wrap(arrayOutputStream.toByteArray());
        outputStreamWriter.close();
        arrayOutputStream.close();
        return b;
    }
}
