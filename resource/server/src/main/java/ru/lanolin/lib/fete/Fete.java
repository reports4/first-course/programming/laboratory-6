package ru.lanolin.lib.fete;

import ru.lanolin.additions.Guest;
import ru.lanolin.lib.enums.Feel;
import ru.lanolin.lib.exception.AddHumanIntoFeteException;
import ru.lanolin.lib.exception.LeaveHumanIntoFeteException;
import ru.lanolin.lib.human.Human;
import ru.lanolin.util.Utils;
import ru.lanolin.util.XML;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class Fete implements DoFete {

    protected final List<Human> guests;
    private boolean invertSort;

    public Fete(File envFile) {
        this.guests = new CopyOnWriteArrayList<>(new ArrayList<>(Utils.MAXSIZE));
        load(envFile);
        invertSort = false;
    }

    public List<Human> getGuests() { return guests; }

    private void load(File envFile) {
        XML xmlParser = new XML(envFile);
        List<Human> guests = null;
        try { guests = xmlParser.readXMLFile(); } 
        catch (IOException | XMLStreamException e) {
            System.err.println("Произошла ошибка при чтении файла.\nОшибка " + e.getClass().getCanonicalName()
                    + " с сообщением: " + e.getLocalizedMessage());
            System.exit(-1);
        }
        addParticipants(guests);
    }

    private Human construct(Map<String, Object> element) {
        Human h = null;

        if (Objects.nonNull(element))
            if (element.size() == 4) {
                try {
                    h = new Guest(
                            element.get("name").toString(),
                            Feel.valueOf(element.get("fate").toString()),
                            Integer.parseInt(element.get("quality").toString()),
                            element.get("birthday").toString()
                    );
                } catch (NullPointerException e) {
                    System.err.println("Не хватает одного из полей или непральное имя поля в ключе");
                } catch (NumberFormatException e1) {
                    System.err.println("Внимание, передано значение в поле, требующие числовое значение, которое не переводится в числовое");
                }
            } else if (element.size() == 2 && element.containsKey("name") && element.containsKey("fate")) {
                try {
                    h = new Guest(element.get("name").toString(), Feel.valueOf(element.get("fate").toString()));
                } catch (NullPointerException e) {
                    System.err.println("Не хватает одного из полей или непральное имя поля в ключе");
                } catch (IllegalArgumentException e1) {
                    System.err.println("В качестве значения Fate было передано такое, которое не существует");
                }
            }

        return h;
    }

    public void info() {
        String builder = String.join("", Collections.nCopies(10, "-")) + System.lineSeparator() +
                "Информация о коллекции: " + System.lineSeparator() +
                String.format("Тип: %s", guests.getClass().getCanonicalName()) + System.lineSeparator() +
                String.format("Максимальное количество элементов: %d", Utils.MAXSIZE) + System.lineSeparator() +
                String.format("Количество элементов в коллекции: %d", guests.size()) + System.lineSeparator() +
                String.format("Заполнение %3.2f%%", (float) guests.size() / Utils.MAXSIZE * 100) + System.lineSeparator() +
                String.join("", Collections.nCopies(10, "-")) + System.lineSeparator();
        System.out.println(builder);
    }

    public void reorder() {
        guests.sort((h1, h2) -> invertSort ? -h1.compareTo(h2) : h1.compareTo(h2) );
        System.out.println("Сортировка выполнена. Колекция отсортирована в порядке " + (invertSort ? "убывания" : "возрастания"));
    }
    
    public void help(){
        String sb = "Справка по командам:" + System.lineSeparator() +
                "{JSON element} - обязательное содержание ключей 'name' и 'fate', " +
                "а также необязательные элементы тип int: 'quality' и 'evaluation'" + System.lineSeparator() +
                "Доступные варианты для ввода в поле 'fate': " + Arrays.asList(Feel.values()).toString() + System.lineSeparator() +
                ">reorder - команда сортирует коллекцию в обратном порядке" + System.lineSeparator() +
                ">info - команда выводит информацию об коллекции" + System.lineSeparator() +
                ">show - команда показывает все элементы коллекции" + System.lineSeparator() +
                ">start - команда запускает генерацию сценария" + System.lineSeparator() +
                ">remove_greater {JSON element} - команда принимает на вход JSON строку, в которой описан " +
                "элемент с которого будет удалены элемента большие этого" + System.lineSeparator() +
                ">add {JSON element} - команда добавляет в коллекцию объект, составленный из JSON строки." + System.lineSeparator() +
                ">remove {JSON element} - удаляет элемент из коллекции." + System.lineSeparator() +
                ">insert {int index} {JSON element} - Добавляет элемент в index позицию." + System.lineSeparator() +
                ">exit/close - Завершение работы приложения." + System.lineSeparator();
        System.out.println(sb);
    }
    
    public void remove_greater(String element) {
        final Human h = construct(Utils.parseJSON(element));
        if (Objects.isNull(h)) {
            String m = "Внимание, невалидный объект не может быть найден в коллекции";
            System.err.println( m);
            return;
        }
        if (guests.indexOf(h) != -1) { guests.removeIf(human -> human.compareTo(h) < 0); System.out.println("Удаление выполнено");
        } else {
            System.err.println("Переданный элемент не может быть найден в коллекции. Либо его нет в коллекции, либо уточните запрос");
        }
    }

    public void add(String element) {
        final Human h = construct(Utils.parseJSON(element));
        if (Objects.isNull(h)) {
            System.err.println("Внимание, невалидный объект не может быть добавлен в коллекцию");
            return;
        }
        try {
            addParticipant(h);
        }catch (AddHumanIntoFeteException add){
            System.err.println(add.getMessage());
            return;
        }
        String m = "Успешно добавлен";
        System.out.println(m);
    }

    public void remove(String element) {
        final Human h = construct(Utils.parseJSON(element));
        if (Objects.isNull(h)) {
            System.err.println("Внимание, невалидный объект не может быть удален из коллекцию");
            return;
        }
        try {
            leaveParticipant(h);
        }catch (LeaveHumanIntoFeteException lhe){
            System.err.println(lhe.getMessage());
            return;
        }
        System.out.println("Успешно удален");
    }

    public void show() {
        StringBuilder builder = new StringBuilder("Вывод всех элементов коллекции: \n");
        guests.forEach(h -> builder.append(h.toPrettyString()).append(System.lineSeparator()));
        System.out.println(builder.toString());
    }

    public void insert(String text) {
        String[] splitText = text.split("\\s+", 2);
        int position;
        try {
            position = Integer.parseInt(splitText[0]);
        } catch (NumberFormatException e) {
            System.err.println("В качестве индекса для вставки элемента было передано невалидное значение. Вставить элемент в коллекцию неудалось");
            return;
        }
        final Human h = construct(Utils.parseJSON(splitText[1]));
        if (Objects.isNull(h)) {
            System.err.println("Внимание, невалидный объект не может быть вставлен в коллекцию");
            return;
        }
        if(guests.contains(h)){
            System.err.println("Внимание, " + new AddHumanIntoFeteException().getMessage());
            return;
        }
        try {
            guests.add(position, h);
        } catch (IndexOutOfBoundsException e) {
            System.err.println("В качествее индекса было введено значения, которое находится вне дозволенных границ." +
                    "Дозволенные границы: [0;" + guests.size() + ")");
        }
    }
}
