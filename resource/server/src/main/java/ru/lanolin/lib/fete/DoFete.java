package ru.lanolin.lib.fete;

import ru.lanolin.lib.human.Human;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public interface DoFete {
    void addParticipant(Human h);
    void leaveParticipant(Human h);
    default void addParticipants(List<Human> hs) { hs.forEach(this::addParticipant); }
    default void leaveParticipants(Vector<Human> guest) {
        for (int i = guest.size() - 1; i > -1; i--) this.leaveParticipant(guest.get(i));
    }
}
