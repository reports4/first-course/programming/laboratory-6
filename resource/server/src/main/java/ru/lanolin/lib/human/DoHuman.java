package ru.lanolin.lib.human;

import ru.lanolin.lib.enums.Phrases;

public interface DoHuman {
    void great(Human other);
    void bowOut(Human other);
    default void repeat(Human other, Phrases phrase) { System.out.println(">" + this + " обращается к " + other.toString() + " и говорить: " + phrase.getPhrases()); }
}
